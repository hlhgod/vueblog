package com.fenghuan.blogapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fenghuan.blogapi.entity.Comment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
public interface CommentService extends IService<Comment> {

}
