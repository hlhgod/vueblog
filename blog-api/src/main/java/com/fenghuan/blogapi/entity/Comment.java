package com.fenghuan.blogapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Getter
@Setter
@TableName("comment")
@ApiModel(value = "Comment对象", description = "")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("评论内容")
    @TableField("content")
    private String content;

    @ApiModelProperty("头像(图片路径)")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("评论时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty("评论者ip地址")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("博主回复")
    @TableField("is_admin_comment")
    private Integer isAdminComment;

    @ApiModelProperty("公开或非公开")
    @TableField("is_published")
    private Boolean isPublished;

    @ApiModelProperty("所属的文章")
    @TableField("blog_id")
    private Long blogId;

    @ApiModelProperty("父评论id，-1为根评论")
    @TableField("parent_comment_id")
    private Long parentCommentId;

    @ApiModelProperty("个人网站")
    @TableField("website")
    private String website;

    @ApiModelProperty("被回复昵称")
    @TableField("parent_comment_nickname")
    private String parentCommentNickname;

    @ApiModelProperty("如果评论昵称为QQ号，则将昵称和头像置为QQ昵称和QQ头像，并将此字段置为QQ号备份")
    @TableField("qq")
    private String qq;


}
