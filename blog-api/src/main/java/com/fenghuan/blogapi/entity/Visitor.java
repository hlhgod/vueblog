package com.fenghuan.blogapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Getter
@Setter
@TableName("visitor")
@ApiModel(value = "Visitor对象", description = "")
public class Visitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("访客标识码")
    @TableField("uuid")
    private String uuid;

    @ApiModelProperty("ip")
    @TableField("ip")
    private String ip;

    @ApiModelProperty("ip来源")
    @TableField("ip_source")
    private String ipSource;

    @ApiModelProperty("操作系统")
    @TableField("os")
    private String os;

    @ApiModelProperty("浏览器")
    @TableField("browser")
    private String browser;

    @ApiModelProperty("首次访问时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty("最后访问时间")
    @TableField("last_time")
    private LocalDateTime lastTime;

    @ApiModelProperty("访问页数统计")
    @TableField("pv")
    private Integer pv;

    @ApiModelProperty("user-agent用户代理")
    @TableField("user_agent")
    private String userAgent;


}
