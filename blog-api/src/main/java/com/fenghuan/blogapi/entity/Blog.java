package com.fenghuan.blogapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Getter
@Setter
@TableName("blog")
@ApiModel(value = "Blog对象", description = "")
public class Blog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("文章标题")
    @TableField("title")
    private String title;

    @ApiModelProperty("文章首图，用于随机文章展示")
    @TableField("first_picture")
    private String firstPicture;

    @ApiModelProperty("描述")
    @TableField("description")
    private String description;

    @ApiModelProperty("文章正文")
    @TableField("content")
    private String content;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty("浏览次数")
    @TableField("views")
    private Integer views;

    @ApiModelProperty("文章字数")
    @TableField("words")
    private Integer words;

    @ApiModelProperty("文章分类id")
    @TableField("type_id")
    private Long typeId;

    @ApiModelProperty("文章作者id")
    @TableField("user_id")
    private Long userId;

    @TableField("status")
    private Integer status;


}
