package com.fenghuan.blogapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fenghuan.blogapi.entity.Friend;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Mapper
public interface FriendMapper extends BaseMapper<Friend> {

}
