package com.fenghuan.blogapi.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@RestController
@RequestMapping("/blogapi/visitor")
public class VisitorController {

}
