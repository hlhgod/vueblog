package com.fenghuan.blogapi.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fenghuan.blogapi.entity.User;
import com.fenghuan.blogapi.mapper.UserMapper;
import com.fenghuan.blogapi.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
