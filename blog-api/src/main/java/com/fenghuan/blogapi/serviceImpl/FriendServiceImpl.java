package com.fenghuan.blogapi.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fenghuan.blogapi.entity.Friend;
import com.fenghuan.blogapi.mapper.FriendMapper;
import com.fenghuan.blogapi.service.FriendService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, Friend> implements FriendService {

}
