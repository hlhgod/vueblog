package com.fenghuan.blogapi.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fenghuan.blogapi.entity.Type;
import com.fenghuan.blogapi.mapper.TypeMapper;
import com.fenghuan.blogapi.service.TypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService {

}
