package com.fenghuan.blogapi.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fenghuan.blogapi.entity.Comment;
import com.fenghuan.blogapi.mapper.CommentMapper;
import com.fenghuan.blogapi.service.CommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

}
