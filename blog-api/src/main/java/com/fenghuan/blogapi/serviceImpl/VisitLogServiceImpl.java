package com.fenghuan.blogapi.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fenghuan.blogapi.entity.VisitLog;
import com.fenghuan.blogapi.mapper.VisitLogMapper;
import com.fenghuan.blogapi.service.VisitLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hlh
 * @since 2021-10-30
 */
@Service
public class VisitLogServiceImpl extends ServiceImpl<VisitLogMapper, VisitLog> implements VisitLogService {

}
